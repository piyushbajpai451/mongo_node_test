var express = require("express");
var app = express();
var port = 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/node-demo");
var nameSchema = new mongoose.Schema({
    firstName: String,
    lastName: String
});
var User = mongoose.model("User", nameSchema);

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});


app.post("/addname", (req, res) => {
    if(req.body.firstName == req.body.lastName)
    {

    var myData = new User(req.body);
    console.log(req.body);
    
    
    myData.save() // this is mongo fn
        .then(item => {
            res.send("Name saved to database");
        })
        .catch(err => {
            res.status(400).send("Unable to save to database");
        });
     }
      else
        {
            // res.send("somthing going wrong");
            //window.alert("hi i think you should know some docs!!");
            
            res.send('something going wrong');

        }
});

app.get('/view', function(req, res)
{

User.findById({} ,function(err, docs)
{
    if(err)
    {
        res.json(err)
    }
    else{
        
        res.json('index',{User: docs});
    }
});

});
app.listen(port, () => {
    console.log("Server listening on port " + port);
});
